<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ProbarConexion', function () {
    try{
    	DB::connection()->getPdo();
    	echo 'Conexion exitosa!';
    }catch(\Exception $e){
    	die("No se puede conectar a la base de datos. Error:". $e);
    }
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
